DOTFILES=${HOME}/.tmux.conf \
	${HOME}/.Xdefaults \
	${HOME}/.doom.d \
	${HOME}/.gitconfig \
	${HOME}/.zshrc ${HOME}/.zlogin ${HOME}/.zlogout ${HOME}/.zprofile ${HOME}/.zshenv \
	${HOME}/.local/bin/test-colors.sh ${HOME}/.local/bin/test-24-bit-colors.sh ${HOME}/.local/bin/edit_with_emacs.sh ${HOME}/.local/bin/stop_emacs_daemon.sh \
	${HOME}/.shrc.d/20-fasd.sh ${HOME}/.shrc.d/20-miniconda.sh ${HOME}/.shrc.d/20-rust.sh \
	${HOME}/.shrc.d/20-lean.sh\
	${HOME}/.shrc.d/20-less.sh ${HOME}/.shrc.d/20-vim.sh ${HOME}/.shrc.d/20-alias.sh ${HOME}/.shrc.d/20-exa.sh \
	${HOME}/.shrc.d/15-file-types.sh ${HOME}/.shrc.d/20-gnome-terminal.sh \
	${HOME}/.shrc.d/20-tmuxinator.sh ${HOME}/.shrc.d/20-direnv.sh ${HOME}/.shrc.d/15-project-root.sh \
	${HOME}/.shrc.d/00-util.sh ${HOME}/.shrc.d/01-emacs.sh ${HOME}/.shrc.d/05-ssh.sh \
	${HOME}/.shrc.d/10-xresources.sh ${HOME}/.shrc.d/10-alacritty-terminal.sh \
	${HOME}/.zshrc.d/00-zsh-util.sh \
	${HOME}/.config/alacritty/alacritty.yml \
	${HOME}/.bash_profile ${HOME}/.bashrc


DOTDIR_LINKS=${HOME}/.emacs.d ${HOME}/.doom.d
DIRS=${HOME}/.local/bin ${HOME}/.zshrc.d ${HOME}/.zfunc ${HOME}/.shrc.d

all: $(DIRS) $(DOTFILES) $(DOTDIR_LINKS)

## Tmux config - there could be something more clever here to 
## select between the styles and attributes config change
${HOME}/.tmux.conf: ${HOME}/dotfiles/tmux.conf
	ln -sf $^ $@

${HOME}/.Xdefaults: $(wildcard ${HOME}/dotfiles/Xdefaults.d/*) $(wildcard ${HOME}/.Xdefaults.d/*) 
	cat > $@ $^

${HOME}/.emacs.d:
	ln -sf ${HOME}/dotfiles/doom-emacs $@

${HOME}/.doom.d:
	ln -sf ${HOME}/dotfiles/doom.d $@

${HOME}/.zshrc: ${HOME}/dotfiles/zshrc
	ln -sf $^ $@

${HOME}/.zlogin: ${HOME}/dotfiles/zlogin
	ln -sf $^ $@

${HOME}/.zlogout: ${HOME}/dotfiles/zlogout
	ln -sf $^ $@

${HOME}/.zprofile: ${HOME}/dotfiles/zprofile
	ln -sf $^ $@

${HOME}/.zshenv: ${HOME}/dotfiles/zshenv
	ln -sf $^ $@

${HOME}/.gitconfig: ${HOME}/dotfiles/gitconfig
	ln -sf $^ $@

${HOME}/.bashrc: ${HOME}/dotfiles/bashrc
	ln -sf $^ $@

${HOME}/.bash_profile: ${HOME}/dotfiles/bash_profile
	ln -sf $^ $@

${HOME}/.zfunc:
	mkdir -p ${HOME}/.zfunc

${HOME}/.zshrc.d:
	mkdir -p ${HOME}/.zshrc.d

${HOME}/.shrc.d:
	mkdir -p ${HOME}/.shrc.d

${HOME}/.shrc.d/00-util.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/00-util.sh
	ln -sf ${HOME}/dotfiles/shrc.d/00-util.sh $@

${HOME}/.shrc.d/01-emacs.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/01-emacs.sh
	ln -sf ${HOME}/dotfiles/shrc.d/01-emacs.sh $@

${HOME}/.shrc.d/05-ssh.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/05-ssh.sh
	ln -sf ${HOME}/dotfiles/shrc.d/05-ssh.sh $@

${HOME}/.shrc.d/10-xresources.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/10-xresources.sh
	ln -sf ${HOME}/dotfiles/shrc.d/10-xresources.sh $@

${HOME}/.shrc.d/10-alacritty-terminal.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/10-alacritty-terminal.sh
	ln -sf ${HOME}/dotfiles/shrc.d/10-alacritty-terminal.sh $@

${HOME}/.shrc.d/20-rust.sh: ${HOME}/.shrc.d ${HOME}/.zfunc ${HOME}/dotfiles/shrc.d/20-rust.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-rust.sh $@

${HOME}/.shrc.d/20-fasd.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-fasd.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-fasd.sh $@

${HOME}/.shrc.d/20-miniconda.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-miniconda.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-miniconda.sh $@

${HOME}/.shrc.d/20-lean.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-lean.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-lean.sh $@

${HOME}/.shrc.d/20-less.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-less.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-less.sh $@

${HOME}/.shrc.d/20-vim.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-vim.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-vim.sh $@

${HOME}/.shrc.d/20-alias.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-alias.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-alias.sh $@

${HOME}/.shrc.d/20-exa.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-exa.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-exa.sh $@

${HOME}/.shrc.d/15-file-types.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/15-file-types.sh
	ln -sf ${HOME}/dotfiles/shrc.d/15-file-types.sh $@

${HOME}/.shrc.d/20-gnome-terminal.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-gnome-terminal.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-gnome-terminal.sh $@

${HOME}/.shrc.d/20-tmuxinator.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-tmuxinator.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-tmuxinator.sh $@

${HOME}/.shrc.d/20-direnv.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/20-direnv.sh
	ln -sf ${HOME}/dotfiles/shrc.d/20-direnv.sh $@

${HOME}/.shrc.d/15-project-root.sh: ${HOME}/.shrc.d ${HOME}/dotfiles/shrc.d/15-project-root.sh
	ln -sf ${HOME}/dotfiles/shrc.d/15-project-root.sh $@

${HOME}/.zshrc.d/00-zsh-util.sh: ${HOME}/.zshrc.d ${HOME}/dotfiles/zshrc.d/00-zsh-util.sh
	ln -sf ${HOME}/dotfiles/zshrc.d/00-zsh-util.sh $@

${HOME}/.local/bin:
	mkdir -p ${HOME}/.local/bin

${HOME}/.local/bin/test-colors.sh: ${HOME}/.local/bin ${HOME}/dotfiles/bin/test-colors.sh
	ln -sf ${HOME}/dotfiles/bin/test-colors.sh $@

${HOME}/.local/bin/test-24-bit-colors.sh: ${HOME}/.local/bin ${HOME}/dotfiles/bin/test-24-bit-colors.sh
	ln -sf ${HOME}/dotfiles/bin/test-24-bit-colors.sh $@

${HOME}/.local/bin/edit_with_emacs.sh: ${HOME}/.local/bin ${HOME}/dotfiles/bin/edit_with_emacs.sh
	ln -sf ${HOME}/dotfiles/bin/edit_with_emacs.sh $@

${HOME}/.local/bin/stop_emacs_daemon.sh: ${HOME}/.local/bin ${HOME}/dotfiles/bin/stop_emacs_daemon.sh
	ln -sf ${HOME}/dotfiles/bin/stop_emacs_daemon.sh $@

${HOME}/.config/alacritty/alacritty.yml: ${HOME}/dotfiles/misc/alacritty.yml
	mkdir -p ${HOME}/.config/alacritty && ln -sf $^ $@

clean:
	rm -f $(DOTDIR_LINKS) $(DOTFILES)
