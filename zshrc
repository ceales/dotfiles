# zshrc executed for each interactive shell

export ACTIVE_SHELL=zsh

source ${HOME}/.shrc.d/00-util.sh

source_shrcd

# Basic history config
HISTFILE=${HOME}/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Write each history item as it happens with extra details such as time and
# date, and snoop file for other new items
setopt INC_APPEND_HISTORY SHARE_HISTORY EXTENDED_HISTORY

# Dont clobber files by redirection, but  edit the history of the rejected
# command so that if you repreat it then it clobbers
setopt HIST_ALLOW_CLOBBER NO_CLOBBER

# Set up handling of dups in the history
setopt HIST_REDUCE_BLANKS HIST_IGNORE_DUPS HIST_EXPIRE_DUPS_FIRST \
       HIST_FIND_NO_DUPS

# Don't put history commands and function definitions in history
setopt HIST_NO_STORE HIST_NO_FUNCTIONS

# Do not execute history commands instead put them into the edit buffer
setopt HIST_VERIFY

# Turn on VI key binding
bindkey -v

# Use the extended "run-help" command and bind it to 'H' in command mode
if alias run-help > /dev/null
then
    unalias run-help
fi
autoload -Uz run-help
bindkey -a H run-help

# Do not quit on ^D it is too easy to type
setopt IGNORE_EOF

# Add a directory for local function definitions
fpath=(${HOME}/.zfunc ${fpath})

# Load auto completion
autoload -Uz compinit

# Turn on menu completion, and enable completion for aliases
zstyle ':completion:*' menu select
zstyle ':completion:*' completer _complete _ignored _approximate
zstyle ':completion:*' max-errors 1
setopt COMPLETE_ALIASES

# Set a fancy prompt - 2 levels of dirs and status of last command
PS1='%(?.%F{green}√.%F{red}?%?)%f %m %B%F{blue}%2~%f%b %# '

# Initialise completions
compinit

# Adjust completion keys as I seem to have messed up somewhere ;)

bindkey -r -- "/"
bindkey -- "." "_history-complete-older"

# If we are dumb then make things simple
[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ '

# Execute local interactive setup
if [ -d "$HOME/.zshrc.d" ]; then
    for f in $(/bin/ls $HOME/.zshrc.d/*.sh | sort); do
        . ${f}
    done
fi

true

# Local Variables:
# mode: sh
# End:
