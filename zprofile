# zprofile executed for all login shell immediately after zshenv

export ACTIVE_SHELL=zsh

source ${HOME}/.shrc.d/00-util.sh
configure_local_path
configure_local_env
configure_locale

# Local Variables:
# mode: sh
# End:
