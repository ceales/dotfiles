#!/usr/bin/env sh

LOCAL_FONT_PATH=${HOME}/.local/share/fonts

mkdir -p ${LOCAL_FONT_PATH}
curl -L https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.tar.xz --output - | tar xJf - -C ${LOCAL_FONT_PATH}

fc-cache
