#!/usr/bin/env bash

set -euo pipefail

curl https://sh.rustup.rs | sh -s -- -c rust-analyzer rust-src rls --no-modify-path -y

rust_analyzer_bin="${HOME}/.cargo/bin/rust-analyzer"
if [ ! -x "${rust_analyzer_bin}" ]
then
    cat > "${rust_analyzer_bin}" <<EOF
#!/usr/bin/env sh
"\$(rustup show home)/toolchains/\$(rustup show active-toolchain | cut '-d ' -f 1)/bin/rust-analyzer" "\$@"
EOF
chmod 700 "${rust_analyzer_bin}"
fi
