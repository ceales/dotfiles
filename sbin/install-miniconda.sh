#!/usr/bin/env bash

set -euo pipefail

install_script=$(mktemp -p ${HOME} miniconda_install_XXX.sh)
install_path=${HOME}/.miniconda
installer_url="https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh"

trap "rm -f ${install_script}" EXIT ERR

echo "Downloading installer [$installer_url] to [${install_script}]"
curl -L ${installer_url} > ${install_script}
chmod 700 ${install_script}

echo "Installing to [${install_path}]"
${install_script} -b -u -p ${install_path}

${install_path}/bin/conda update -n base -c defaults conda
