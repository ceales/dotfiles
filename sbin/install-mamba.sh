#!/usr/bin/env bash

set -euo pipefail

install_script=$(mktemp -p ${HOME} mamba_install_XXX.sh)
install_path=${HOME}/.mamba
installer_url="https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-Linux-x86_64.sh"

trap "rm -f ${install_script}" EXIT ERR

echo "Downloading installer [$installer_url] to [${install_script}]"
curl -L ${installer_url} > ${install_script}
chmod 700 ${install_script}

echo "Installing to [${install_path}]"
${install_script} -b -u -p ${install_path}
