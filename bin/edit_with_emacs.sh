#!/bin/bash

source ${HOME}/.shrc.d/00-util.sh
source ${HOME}/.shrc.d/01-emacs.sh

edit_with_emacs "$@"
