#!/usr/bin/env sh

awk 'BEGIN{
    s="/\\/\\/\\/\\/\\"; s=s s s s s s s s s s s s s s s s s s s s ;
    for (colnum = 0; colnum<192; colnum++) {
        r = 255-(colnum*255/191);
        g = (colnum*510/191);
        b = (colnum*255/191);
        if (g>255) g = 510-g;
        printf "\033[48;2;%d;%d;%dm", r,g,b;
        printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
        printf "%s\033[0m", substr(s,colnum+1,1);
    }
    printf "\n";
}'

# Local Variables:
# mode: sh
# End:
