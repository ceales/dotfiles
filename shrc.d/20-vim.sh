# Alias vi to vim if vi not installed

if ! which vi > /dev/null 2>&1 && which vim > /dev/null 2>&1
then
    alias vi=vim
fi
