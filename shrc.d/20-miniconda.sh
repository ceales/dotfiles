# If miniconda exists then setup the normal paths etc

if [ -d "${HOME}/.miniconda" ]
then
    CONDA_PATH=${HOME}/.miniconda
    CONDA_EXE=conda
elif [ -d "${HOME}/.mamba" ]
then
    CONDA_PATH=${HOME}/.mamba
    CONDA_EXE=conda
fi

if [ -n "${CONDA_PATH}" ]
then
    CONDA_BIN=${CONDA_PATH}/bin/${CONDA_EXE}
    CONDA_PROFILE_D=${CONDA_PATH}/etc/profile.d

    if [ ! -f "${HOME}/.condarc" ]
    then
        ${CONDA_BIN} config --set auto_activate_base false
        ${CONDA_BIN} config --add channels conda-forge
        ${CONDA_BIN} config --set channel_priority strict
    fi

    case "${ACTIVE_SHELL}" in
        zsh)
            __conda_setup="$(${CONDA_BIN} 'shell.zsh' 'hook' 2> /dev/null)"
            ;;
        bash)
            __conda_setup="$(${CONDA_BIN} 'shell.bash' 'hook' 2> /dev/null)"
            ;;
        *)
            echo "Unknown active shell [${ACTIVE_SHELL}]"
            exit 1
            ;;
    esac

    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "${CONDA_PROFILE_D}/conda.sh" ]; then
            . "${CONDA_PROFILE_D}/conda.sh"
        else
            export PATH="${CONDA_PATH}/bin):$PATH"
            normalise_path
        fi
    fi

   unset __conda_setup
fi
