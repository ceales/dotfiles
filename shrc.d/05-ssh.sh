# Pull SSH_AUTH_SOCK varibale into local shell

function fix_ssh_auth_sock {
    if [ -n "$SSH_AUTH_SOCK" -a ! -L "$SSH_AUTH_SOCK" -a -S "$SSH_AUTH_SOCK" ]
    then
        ln -sf $SSH_AUTH_SOCK ${HOME}/.ssh/ssh_auth_sock
        export SSH_AUTH_SOCK=${HOME}/.ssh/ssh_auth_sock
    fi
}

function sshagent {
    if pkill ssh-agent
    then
        unset SSH_AUTH_SOCK SSH_AGENT_PID
    fi

    if eval $(ssh-agent)
    then
        fix_ssh_auth_sock
    fi
}
