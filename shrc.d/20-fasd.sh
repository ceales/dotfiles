# Enable fasd if available
# https://github.com/clvv/fasd

if which fasd > /dev/null 2>&1
then
    case "${ACTIVE_SHELL}" in
        zsh)
            eval "$(fasd --init posix-alias zsh-hook zsh-ccomp zsh-ccomp-install \
                zsh-wcomp zsh-wcomp-install)"
            ;;
        bash)
            eval "$(fasd --init posix-alias bash-hook bash-ccomp bash-ccomp-install)"
            ;;
        *)
            echo "Unknown active shell [${ACTIVE_SHELL}]"
            exit 1
            ;;
    esac

    alias ee="f -e ${HOME}/bin/edit_with_emacs.sh"
fi
