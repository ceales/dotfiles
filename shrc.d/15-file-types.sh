export ARCHIVE_FILE_GLOBS=('*.[gx]z' '*.tar' '*.zip' '*.Z' '*.bz2' '*.7z')
export SOURCE_FILE_GLOBS=('*.c' '*.h' '*.[ch]pp' '*.C' '*.java' '*.py' '*.clj' '*.clj[cs]' '*.edn' '*.js' '*.rs' '*.sh')
export PACKAGE_FILE_GLOBS=('*.rpm' '*.deb')
export MEDIA_FILE_GLOBS=('*.mp3' '*.mpeg')

# Local Variables:
# mode: sh
# End:
