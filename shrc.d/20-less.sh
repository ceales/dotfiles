# Swap in less as the PAGER when available

if which less > /dev/null 2>&1
then
    export PAGER=$(which less)
fi
