function get_shell_name () {
    ps -q $$ -o comm=
}

# This has to be an alias otherwise it just returns then name of the function
alias get_executing_file='case $(get_shell_name) in zsh) echo "${(%):-%N}" ;; bash) echo "${BASH_SOURCE}" ;; *) return 1  ;; esac'

function get_effective_user () {
    id -u -n
}

function remove_dups() {
    local seen
    typeset -A seen
    while read value
    do
        if [ -z "${seen[$value]}" ]
        then
            echo $value
            seen[$value]=1
        fi
    done
}

function normalise_path() {
    PATH=$(echo $PATH | tr ':' '\n' | remove_dups | tr '\n' ':')
    PATH=${PATH%:}
}

function configure_local_path() {
    # Add the modern ${HOME}/.local/bin if present
    if [ -d "$HOME/.local/bin" ]; then
        PATH="$HOME/.local/bin:$PATH"
    fi

    if [ -d "$HOME/.path.d" ]; then
        for f in $HOME/.path.d/*; do
            while read pl; do
                if [ -n "$pl" -a "$(echo $pl | cut -c 1)" != "#" ]; then
                    PATH=$pl:$PATH
                fi
            done < $f
        done
    fi

    if [ -d "$HOME/.emacs.d/bin" ]; then
        PATH="$HOME/.emacs.d/bin:$PATH"
    fi

    normalise_path
}

function configure_local_env() {
    if [ -d "$HOME/.env.d" ]; then
        for f in $HOME/.env.d/*; do
            . ${f}
        done
    fi
}

function configure_locale() {
    if locale -a | grep -q en_GB.utf8 ; then
        export LANG=en_GB.utf8
    fi
}

function source_shrcd() {
    if [ -d "$HOME/.shrc.d" ]; then
        for f in $(/bin/ls $HOME/.shrc.d/[0-9][0-9]*.sh | sort); do
            #echo "Sourcing [${f}]"
            . ${f}
        done
    fi
}

# Local Variables:
# mode: sh
# End:
