# Enable zsh completions for rust

if which rustup > /dev/null 2>&1
then
    case "${ACTIVE_SHELL}" in
        zsh)
            rustup completions zsh rustup >| ${HOME}/.zfunc/_rustup
            rustup completions zsh cargo >| ${HOME}/.zfunc/_cargo
            ;;
        bash)
            mkdir -p ${HOME}/.bash_completions
            rustup completions bash rustup > ${HOME}/.bash_completions/rustup
            rustup completions bash cargo > ${HOME}/.bash_completions/cargo
            source ${HOME}/.bash_completions/rustup
            source ${HOME}/.bash_completions/cargo
            ;;
        *)
            echo "Unknown active shell [${ACTIVE_SHELL}]"
            exit 1
            ;;
    esac
fi
