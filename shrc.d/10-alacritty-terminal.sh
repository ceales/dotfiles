if [ "${TERM}" = "alacritty" ]
then
   if whence infocmp > /dev/null 2> /dev/null
   then
      if infocmp alacritty-direct > /dev/null 2> /dev/null
      then
          TERM=alacritty-direct
      fi
   fi
fi
