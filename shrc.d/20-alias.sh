# Add alias for launching emacsclient
alias e=edit_with_emacs

# Turn on colors for gnu apps in terminal
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

alias diff="diff --color=auto"
alias ip="ip --color=auto"
