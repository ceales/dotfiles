if which direnv > /dev/null 2>&1
then
    DIRENV_CONFIG=$(direnv status | grep 'DIRENV_CONFIG' | cut '-d ' -f 2)
    if [ -n "${PROJECTS_ROOT}" -a  ! -d "${DIRENV_CONFIG}" ]
    then
        export DIRENV_CONFIG="${PROJECTS_ROOT}/.config/direnv"
        if [ ! -d ${DIRENV_CONFIG} ]
        then
            mkdir -p ${DIRENV_CONFIG}
        fi
    fi
    case "${ACTIVE_SHELL}" in
        zsh)
        eval "$(direnv hook zsh)"
            ;;
        bash)
        eval "$(direnv hook bash)"
            ;;
        *)
            echo "Unknown active shell [${ACTIVE_SHELL}]"
            exit 1
            ;;
    esac
    normalise_path
fi
