# If lean/elan installed then configure

if [ -d ${HOME}/.elan ]
then
    source ${HOME}/.elan/env
    if which elan > /dev/null 2>&1
    then
        case "${ACTIVE_SHELL}" in
             zsh)
                 elan completions zsh >| ${HOME}/.zfunc/_elan
                 ;;
             bash)
                 __lean_setup="$(elan completions bash 2> /dev/null)"
                 if [ $? -eq 0  ]; then
                     eval "$__lean_setup"
                 fi
                 unset __lean_setup
                 ;;
             *)
                 echo "Unknown active shell [${ACTIVE_SHELL}]"
                 exit 1
                 ;;
        esac
    fi
fi
