function get_emacs_daemon_pid () {
    EUSER=$(id)
    if ! PIDS=$(pgrep -x -f "emacs --daemon" -u $(get_effective_user)) ; then return 1 ; fi
    if [ 1 -ne $(echo "${PIDS}" | wc -l) ]
    then
        return 2
    fi
    echo "${PIDS}"
}

function edit_with_emacs () {
    if get_emacs_daemon_pid > /dev/null
    then
        if [ -z ${DISPLAY+x} ]
        then
            emacsclient -t "$@"
        else
            emacsclient -c "$@"
        fi
    else
        emacs --daemon && edit_with_emacs "$@"
    fi
}

function eval_with_emacs () {
    edit_with_emacs -e "$*"
}

function stop_emacs_daemon () {
    get_emacs_daemon_pid > /dev/null && eval_with_emacs "(kill-emacs)"
}

function diff_with_emacs() {
    # test args
    if [ ! ${#} -ge 2 ]; then
        echo 1>&2 "Usage: ${0} LOCAL REMOTE [MERGED BASE]"
        echo 1>&2 "       (LOCAL, REMOTE, MERGED, BASE can be provided by \`git mergetool'.)"
        exit 1
    fi

    # tools
    _BASENAME=/bin/basename
    _CP=/bin/cp
    _EGREP=/bin/egrep
    _MKTEMP=/bin/mktemp

    # args
    _LOCAL=${1}
    _REMOTE=${2}
    if [ ${3} ] ; then
        _MERGED=${3}
    else
        _MERGED=${_REMOTE}
    fi
    if [ ${4} -a -r ${4} ] ; then
        _BASE=${4}
        _EDIFF=ediff-merge-files-with-ancestor
        _EVAL="${_EDIFF} \"${_LOCAL}\" \"${_REMOTE}\" \"${_BASE}\" nil \"${_MERGED}\""
    elif [ ${_REMOTE} = ${_MERGED} ] ; then
        _EDIFF=ediff
        _EVAL="${_EDIFF} \"${_LOCAL}\" \"${_REMOTE}\""
    else
        _EDIFF=ediff-merge-files
        _EVAL="${_EDIFF} \"${_LOCAL}\" \"${_REMOTE}\" nil \"${_MERGED}\""
    fi

    eval_with_emacs "(${_EVAL})" 2>&1
    #
    # check modified file
    if [ ! $(egrep -c '^(<<<<<<<|=======|>>>>>>>|####### Ancestor)' ${_MERGED}) = 0 ]; then
        _MERGEDSAVE=$(${_MKTEMP} --tmpdir `${_BASENAME} ${_MERGED}`.XXXXXXXXXX)
        ${_CP} ${_MERGED} ${_MERGEDSAVE}
        echo 1>&2 "Oops! Conflict markers detected in $_MERGED."
        echo 1>&2 "Saved your changes to ${_MERGEDSAVE}"
        echo 1>&2 "Exiting with code 1."
        exit 1
    fi

    exit 0
}

# Local Variables:
# mode: sh
# End:
