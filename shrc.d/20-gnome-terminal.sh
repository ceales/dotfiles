# Load gnome terminal settings

function load_gnome_terminal_settings {
   < $1 dconf load /org/gnome/terminal/legacy/profiles:/:$2/
}
