if [ -n "${DISPLAY}" -a -f "${HOME}/.Xdefaults" ]
then
    if whence xrdb 2> /dev/null > /dev/null
    then
        xrdb -load ${HOME}/.Xdefaults
    fi
fi
