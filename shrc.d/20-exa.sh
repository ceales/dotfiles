# If exa is installed then use that for ls otherwise we can just add colours to
# the normal ls

if which exa > /dev/null 2>&1
then
    alias l="exa --group-directories-first --git"
    alias ls="l -F"

    alias tree="ls -T"

    EXA_COLORS=""
    for glob in ${ARCHIVE_FILE_GLOBS[@]}
    do
        EXA_COLORS="${EXA_COLORS}:${glob}=4;33"
    done
    for glob in ${SOURCE_FILE_GLOBS[@]}
    do
        EXA_COLORS="${EXA_COLORS}:${glob}=32"
    done
    for glob in ${PACKAGE_FILE_GLOBS[@]}
    do
        EXA_COLORS="${EXA_COLORS}:${glob}=36"
    done
    for glob in ${MEDIA_FILE_GLOBS[@]}
    do
        EXA_COLORS="${EXA_COLORS}:${glob}=34"
    done
    export EXA_COLORS

else
    alias l="/bin/ls"
    alias ls="l --color=auto -F"
fi

alias ll="ls -l"
alias la="ls -a"
alias l1="l -1"
