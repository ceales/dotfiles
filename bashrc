export ACTIVE_SHELL=bash

source ${HOME}/.shrc.d/00-util.sh

[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

configure_locale
source_shrcd

# vi keys obviously
set -o vi
