# bash_profile executed for all login shell immediately after zshenv

export ACTIVE_SHELL=bash

source ${HOME}/.shrc.d/00-util.sh
configure_local_path
configure_local_env

source ${HOME}/.bashrc

# Local Variables:
# mode: sh
# End:
